<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(empty($arResult))
    return "";

$return = '<div class="hero__breadcrumbs breadcrumbs">';

$lastItem = array_pop($arResult);

foreach ($arResult as $arItem) {
    $return .= '<div class="breadcrumbs__item">
                    <a class="breadcrumbs__link" href="'.$arItem['LINK'].'">'.$arItem['TITLE'].'</a>
                </div>';
}

$return .= '<div class="breadcrumbs__item">
                <a class="breadcrumbs__link breadcrumbs__link_current" href="'.$lastItem['LINK'].'">'.$lastItem['TITLE'].'</a>
            </div>';

$return .= '</div>';

return $return;