<?
$this->setFrameMode(true);
?>
<?if (!empty($arResult['ITEMS'])):?>
    <?foreach ($arResult['ITEMS'] as $k => $arItem):

        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));
        ?>
        <?
        // большая карточка
        if ($k == 1):?>
            <div class="video__card video__card--lg show-card" id="<?=$this->GetEditAreaID($arItem['ID'])?>">
                <a class="show-card__link" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
                    <div class="show-card__cover">
                        <img class="show-card__pic" src="<?=(!empty($arItem['PROPERTIES']['POPULAR_PICTURE']['VALUE'])) ? CFile::GetPath($arItem['PROPERTIES']['POPULAR_PICTURE']['VALUE']) : $arItem['PREVIEW_PICTURE']['SRC']?>">
                        <div class="show-card__play">
                            <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                            </svg>
                        </div>
                    </div>
                    <div class="show-card__info">
                        <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                        <div class="show-card__time title title_h4"><?=$arItem['PROPERTIES']['VIDEO_LENGTH']['VALUE']?></div>
                    </div>
                </a>
            </div>
        <?
        // остальные карточки
        else:?>
            <div class="video__card show-card" id="<?=$this->GetEditAreaID($arItem['ID'])?>">
                <a class="show-card__link" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
                    <div class="show-card__cover">
                        <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                        <div class="show-card__play">
                            <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                            </svg>

                        </div>
                    </div>
                    <div class="show-card__info">
                        <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                        <div class="show-card__time title title_h4"><?=$arItem['PROPERTIES']['VIDEO_LENGTH']['VALUE']?></div>
                    </div>
                </a>
            </div>
        <?endif;?>
    <?endforeach;?>
    <?=$arResult['NAV_STRING']?>
<?endif;?>
