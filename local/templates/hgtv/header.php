<?
$asset = Bitrix\Main\Page\Asset::getInstance();

Bitrix\Main\Loader::includeModule('air.site');
?><!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon.ico" sizes="16x16">
    <title><?$APPLICATION->ShowTitle()?></title>
    <?
    // css
    $asset->addCss(SITE_TEMPLATE_PATH.'/assets/css/app.css');

    // js
    $asset->addJs(SITE_TEMPLATE_PATH.'/assets/js/foundation.js');
    $asset->addJs(SITE_TEMPLATE_PATH.'/assets/js/app.js');

    $APPLICATION->ShowHead();
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-166332017-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-166332017-1');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MSTRNZP');</script>
    <!-- End Google Tag Manager -->
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(62772919, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true,
            ecommerce:"dataLayer"
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/62772919" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MSTRNZP"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?$APPLICATION->ShowPanel()?>
    <div class="wrapper">
        <header class="header">
            <div class="header__container container">
                <div class="header__left">
                    <div class="header__left-container">
                        <a class="header__logo" href="/">
                            <?$APPLICATION->IncludeFile('/include/index/logo.php', false, ['SHOW_BORDER' => false])?>
                        </a>
                        <div class="header__controls">
                            <div class="header__search-icon-mob">
                                <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M16.8999 25.4C12.2055 25.4 8.3999 21.5944 8.3999 16.9C8.3999 12.2056 12.2055 8.40002 16.8999 8.40002C21.5943 8.40002 25.3999 12.2056 25.3999 16.9C25.3999 18.8869 24.7182 20.7145 23.576 22.1618L28.5071 27.0929C28.8976 27.4834 28.8976 28.1166 28.5071 28.5071C28.1166 28.8976 27.4834 28.8976 27.0929 28.5071L22.1618 23.576C20.7145 24.7183 18.8868 25.4 16.8999 25.4ZM16.8998 23.4C20.4896 23.4 23.3998 20.4899 23.3998 16.9C23.3998 13.3102 20.4896 10.4 16.8998 10.4C13.3099 10.4 10.3998 13.3102 10.3998 16.9C10.3998 20.4899 13.3099 23.4 16.8998 23.4Z" fill="black"/>
                                </svg>
                            </div>
                            <div class="header__burger burger"><span></span></div>
                        </div>
                    </div>
                </div>
                <div class="header__right">
                    <div class="header__right-top">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:search.title",
                            "",
                            Array(
                                "CATEGORY_0" => array("iblock_content"),
                                "CATEGORY_0_TITLE" => "",
                                "CATEGORY_0_forum" => array("all"),
                                "CATEGORY_0_iblock_content" => array("3","8"),
                                "CATEGORY_0_iblock_system" => array("all"),
                                "CHECK_DATES" => "N",
                                "CONTAINER_ID" => "title-search",
                                "INPUT_ID" => "title-search-input",
                                "NUM_CATEGORIES" => "1",
                                "ORDER" => "date",
                                "PAGE" => "#SITE_DIR#search/index.php",
                                "SHOW_INPUT" => "Y",
                                "SHOW_OTHERS" => "N",
                                "TOP_COUNT" => "5",
                                "USE_LANGUAGE_GUESS" => "Y"
                            )
                        );?>
                        <div class="header__socials socials">
                            <?$APPLICATION->IncludeFile('/include/index/social.php', false, ['SHOW_BORDER' => false])?>
                        </div>
                    </div>
                    <div class="header__right-bottom nav">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "main_top",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "top",
                                "USE_EXT" => "N"
                            )
                        );?>
                        <div class="header__smaller-menu">
                            <div class="nav nav_column">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "main_top",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "left",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "1",
                                        "MENU_CACHE_GET_VARS" => array(""),
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_TYPE" => "N",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "ROOT_MENU_TYPE" => "top",
                                        "USE_EXT" => "N"
                                    )
                                );?>
                            </div>
                            <div class="socials socials_tablet">
                                <?$APPLICATION->IncludeFile('/include/index/social.php', false, ['SHOW_BORDER' => false])?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
