<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
$maxScheduleDay = 7;
?>
<div class="on-air">
    <div class="on-air__container container container_inner">
        <div class="on-air__left">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "dark",
                Array(
                    "PATH" => "",
                    "SITE_ID" => "s1",
                    "START_FROM" => "0"
                )
            );?>
            <div class="on-air__heading title"><?$APPLICATION->ShowTitle(false)?></div>
        </div>
        <div class="on-air__right now">
            <div class="now__container">
                <div class="now__left">
                    <div class="now__text">Сейчас в эфире</div>
                    <div class="now__show-name"><?=$arResult['ON_AIR']['NAME']?></div>
                    <?/*
                    <a class="now btn btn_blue-filled" href="#">
                        <div class="btn__link">не пропустить</div>
                    </a>
                    */?>
                </div>
                <div class="now__right">
                    <?/*
                    <div class="now__video">
                        <div class="show-card">
                            <a class="show-card__link" href="//www.youtube.com/watch?v=T_6CeJCad6Y&amp;list=LLlHMjNwXSCANxW6EfndN0rg&amp;index=11&amp;t=68s" data-lity>
                                <div class="show-card__cover show-card__cover_on-air">
                                    <img class="show-card__pic" src="assets/img/vid-9.jpg">
                                    <div class="show-card__play show-card__play_decor">
                                        <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                                        </svg>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <a class="now btn btn_blue-filled" href="#">
                            <div class="btn__link">не пропустить</div>
                        </a>
                    </div>
                    */?>
                </div>
            </div>
        </div>
    </div>
</div>
<?if (!empty($arResult['ERRORS'])):?>
    <?foreach ($arResult['ERRORS'] as $error):?>
        <p class="error"><?=$error?></p>
    <?endforeach?>
<?else:?>
    <div class="day-slider">
        <div class="day-slider__box swiper-container">
            <ul class="day-slider__list slider-top swiper-wrapper">
                <?
                $curDate = date($arResult['DAY_FORMAT']);
                $k = 1;
                ?>
                <?foreach ($arResult['SCHEDULE'] as $day => $schedule):
                    if ($k > $maxScheduleDay) break;
                    ?>
                    <li class="day-slider__item swiper-slide<?if ($day == $curDate):?> active<?endif?>" data-tab-select="<?=$k?>">
                        <div class="day-slider__content">
                            <div class="day-slider__weekday"><?=\Air\Site\Schedule::getDayName($day)?></div>
                            <div class="day-slider__date"><?=date('d/m', strtotime("$day.".date('Y')))?></div>
                        </div>
                    </li>
                    <?
                    $k++;
                    ?>
                <?endforeach?>
            </ul>
        </div>
    </div>
    <div class="shows-list">
        <div class="shows-list__container">
            <div class="shows-list__wrapper-for-list">
                <?
                $k = 1;
                ?>
                <?foreach ($arResult['SCHEDULE'] as $schedule):
                    if ($k > $maxScheduleDay) break;
                    ?>
                    <ul class="shows-list__shows-accordion accordion<?if ($k == 2):?> active<?endif?>" data-tab="<?=$k?>">
                        <?foreach ($schedule as $broadcast):?>
                            <?if ($broadcast['PASSED'] == 'Y' && $broadcast['PASSED_SHOW'] != 'Y') continue;?>
                            <li class="accordion__block show-cell<?if ($broadcast['PASSED_SHOW'] == 'Y'):?> show-cell_passed<?endif?>">
                                <div class="accordion__action show-cell__heading">
                                    <div class="show-cell__left">
                                        <div class="show-cell__time"><?=$broadcast['TIME_START']?></div>
                                        <?if ($broadcast['ON_AIR'] == 'Y'):?>
                                            <div class="show-cell__prime">В эфире</div>
                                        <?endif?>
                                    </div>
                                    <div class="show-cell__right">
                                        <? /*
                                        <div class="show-cell__pic-heading"><img class="show-cell__img-heading" src="assets/img/vid-1.jpg"></div>
                                        */?>
                                        <div class="show-cell__overview">
                                            <div class="show-cell__title"><?=$broadcast['NAME']?></div>
                                            <div class="show-cell__season">СЕЗОН <?=$broadcast['SEASON']?>, СЕРИЯ <?=$broadcast['EPISODE']?></div>
                                        </div>
                                    </div>
                                    <div class="show-cell__trigger">
                                        <svg width="15" height="9" viewBox="0 0 15 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13.963 1.82716L7.48148 8L1 1.82716" stroke="#3CB9D2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="accordion__sliding-part">
                                    <div class="show-about">
                                        <div class="show-about__wrapper">
                                            <div class="show-about__text"><?=$broadcast['PREVIEW_TEXT']?></div>
                                            <div class="show-about__pic"><img class="show-about__img" src="assets/img/vid-2.jpg"></div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?endforeach?>
                    <?
                    $k++;
                    ?>
                    </ul>
                <?endforeach?>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var swiperOptions7 = {
                direction: 'horizontal',
                breakpoints: {
                    316: {
                        slidesPerView: 2,
                    },
                    532: {
                        slidesPerView: 4,
                    },
                    900: {
                        slidesPerView: 4
                    },
                    1217: {
                        slidesPerView: 5
                    },
                    1430: {
                        slidesPerView: 6
                    }
                },
                mousewheelControl: true,
                keyboardControl: true,
                on: {},
                initialSlide: $('.day-slider__item.active').length ? $('.day-slider__item.active').index() : 0
            };

            var swiperDays = new Swiper('.day-slider__box', swiperOptions7);

            $('.day-slider__content').on('click', function (e) {
                    $(this).closest('.day-slider__item').addClass('active').siblings().removeClass('active');
                }
            );

            // tabs
            $('[data-tab-select]').on('click', function () {
                var tabNum = $(this).attr('data-tab-select');

                $('[data-tab]').removeClass('active');
                $('[data-tab="'+tabNum+'"]').addClass('active');
            });
        });
    </script>
<?endif?>