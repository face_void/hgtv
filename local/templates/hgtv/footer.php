    </div>
    <footer class="footer">
        <div class="footer__container container container_inner">
            <div class="footer__left">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "main_bottom",
                    Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "bottom",
                        "USE_EXT" => "N"
                    )
                );?>
            </div>
            <div class="footer__copyright-container">
                <div class="footer__copyright">&copy; <?=date('Y')?> HGTV. Все права защищены. Копирование материалов без согласования запрещено.</div>
                <div class="footer__air-prod">
                    <div class="footer__air-logo">Сделано в&nbsp;<a class="footer__logo-link" href="https://air.agency" target="_blank">Air Production</a></div>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>