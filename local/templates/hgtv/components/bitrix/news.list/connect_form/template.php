<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);

$GLOBALS['firstCityId'] = $arResult['ITEMS'][0]['ID'];
?>
<form class="hero__select select" name="connect_form" action="<?=$APPLICATION->GetCurPage()?>">
    <div class="select__name title title_no-m"><?$APPLICATION->IncludeFile('/include/connect/form_title.php', false, ['MODE' => 'html', 'NAME' => 'Заголовок'])?></div>
    <div class="select__input">
        <select class="select2 select__select2 select2_custom select2--opaque" type="search" name="city">
            <?foreach ($arResult['ITEMS'] as $arItem):?>
                <option value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option>
            <?endforeach?>
        </select>
    </div>
    <div class="select__button">
        <button class="btn" type="submit"><span class="btn__link">Ваш провайдер</span></button>
    </div>
</form>