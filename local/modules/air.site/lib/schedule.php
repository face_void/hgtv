<? namespace Air\Site;

use Bitrix\Main\Config\Option;
use Bitrix\Main\UI\Filter\DateTime;

class Schedule
{
    public static function getScheduleArray()
    {
        $scheduleLink = Option::get('air.site', 'schedule_link', '');

        if (empty($scheduleLink))
            return false;

        $schedule = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/upload/schedule.xml');

        if (empty($schedule))
            return false;

        $schedule = new \SimpleXMLElement($schedule);

        $arReturn = [];
        foreach ($schedule->BROADCAST as $broadcast) {
            $arReturn[] = [
                'SHOW_ID' => (string) $broadcast->PROGRAMME['PROGRAMME_ID'],
                'NAME' => (string) $broadcast->BROADCAST_TITLE,
                'TIME_START' => (string) $broadcast->BROADCAST_START_DATETIME,
                'TIME_END' => (string) $broadcast->BROADCAST_END_TIME,
                'SEASON' => (string) $broadcast->PROGRAMME->SERIES_NUMBER,
                'EPISODE' => (string) $broadcast->PROGRAMME->EPISODE_NUMBER,
                'PREVIEW_TEXT' => (string) $broadcast->PROGRAMME->TEXT->TEXT_TEXT
            ];
        }

        return $arReturn;
    }

    public static function getDayName($date)
    {
        $arDayNames = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

        $dateString = "$date.".date('Y');
        $curDate = new \DateTime();

        if ($curDate->format('d.m.Y') == $dateString) {
            $returnStr = 'Сегодня';
        } elseif ($curDate->modify('+1 day')->format('d.m.Y') == $dateString) {
            $returnStr = 'Завтра';
        } else {
            $returnStr = $arDayNames[ date('w', strtotime($dateString)) ];
        }

        return $returnStr;
    }

    function refreshSchedule()
    {
        $scheduleLink = Option::get('air.site', 'schedule_link', '');

        if (empty($scheduleLink))
            return false;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $scheduleLink);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $schedule = curl_exec($ch);
        curl_close($ch);

        if ($schedule === false)
            return false;

        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/schedule.xml', $schedule);

        return 'Air\Site\Schedule::refreshSchedule();';
    }
}