$(document).ready(function () {
    $(document).on('click', '[data-ajax-video]', function (e) {
        e.preventDefault();
        var $this = $(this),
            container = $('.new-video__wrapper');

        $this.closest('.ajax-navigation').append('<div class="new-video__preloader-wrap"><div class="new-video__preloader-shadow"></div><div class="new-video__preloader preloader"><div class="lds-dual-ring"></div></div></div>')

        $.ajax({
            url: $this.attr('href'),
            dataType: 'html',
            success: function (result) {
                $this.closest('.video-list__more').remove();
                $('.new-video__preloader-wrap').remove();
                container.append(result);
            }
        })
    })
});