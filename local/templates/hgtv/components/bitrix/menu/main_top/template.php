<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<div class="nav__container">
    <?foreach ($arResult as $item):?>
        <div class="nav__item">
            <a class="nav__link<?if ($item['SELECTED']):?> active<?endif?>" href="<?=$item['LINK']?>"><?=$item['TEXT']?></a>
        </div>
    <?endforeach?>
</div>
