<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arResult['NavPageNomer'] < $arResult['NavPageCount']):?>
    <div class="video-list__more">
        <a class="btn btn_blue-filled video-list__btn" href="/ajax/new_video.php?PAGEN_1=<?=$arResult['NavPageNomer']+1?>" data-ajax-video>Показать еще</a>
    </div>
<?endif;?>
