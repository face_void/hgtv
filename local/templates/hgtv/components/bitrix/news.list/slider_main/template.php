<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<div class="page-wrap">
    <div class="container" id="home-slider">
        <div id="home-slider__left">
            <div class="swiper-container slider-hero-left">
                <div class="swiper-wrapper">
                    <?
                    // первый элемент должен совпадать, остальные элементы должны идти в обратном порядке
                    $arFirstSlider = [$arResult['ITEMS'][0]];
                    $arFirstSlider = array_merge($arFirstSlider, array_reverse(array_slice($arResult['ITEMS'], 1)));
                    ?>
                    <?foreach ($arFirstSlider as $arItem):?>
                        <div class="swiper-slide">
                            <div class="swiper-image" data-swiper-parallax-y="30%">
                                <div class="swiper-image__inner" style="background-image: url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>)"></div>
                            </div>
                        </div>
                    <?endforeach?>
                </div>
            </div>
        </div>
        <div id="home-slider__right">
            <div class="swiper-container slider-hero-right">
                <div class="swiper-wrapper">
                    <?foreach ($arFirstSlider as $arItem):?>
                        <div class="swiper-slide">
                            <div class="swiper-image" data-swiper-parallax-y="30%">
                                <div class="swiper-image__inner" style="background-image: url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>)"></div>
                            </div>
                        </div>
                    <?endforeach?>
                </div>
            </div>
        </div>
        <div class="swiper-container slider-hero">
            <div class="swiper-wrapper">
                <?foreach ($arResult['ITEMS'] as $arItem):

                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));
                    ?>
                    <div class="swiper-slide" id="<?=$this->GetEditAreaID($arItem['ID'])?>">
                        <div class="swiper-image" data-swiper-parallax-y="35%">
                            <div class="swiper-image__mob-bg" style="background-image: url(<?=$arItem['DETAIL_PICTURE']['SRC']?>)"></div>
                            <div class="swiper-image__inner" style="background-image: url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>)">
                                <div class="swiper-image__top">
                                    <div class="swiper-image__text-wrap">
                                        <div class="swiper-image__heading swiper-image__text-inner"><?=$arItem['NAME']?></div>
                                    </div>
                                    <?if (!empty($arItem['PREVIEW_TEXT'])):?>
                                        <div class="swiper-image__text-wrap">
                                            <p class="swiper-image__paragraph swiper-image__text-inner"><?=$arItem['PREVIEW_TEXT']?></p>
                                        </div>
                                    <?endif?>
                                </div>
                                <div class="swiper-image__bot">
                                    <?if (!empty($arItem['PROPERTIES']['LINK']['VALUE'])):?>
                                        <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" class="btn">
                                            <div class="btn__link"><?=(!empty($arItem['PROPERTIES']['LINK_TITLE']['VALUE'])) ? $arItem['PROPERTIES']['LINK_TITLE']['VALUE'] : 'смотреть первым'?></div>
                                        </a>
                                    <?endif?>
                                    <div class="swiper-image__date-time swiper-image__text-wrap">
                                        <?if (!empty($arItem['PROPERTIES']['DATE']['VALUE'])):?>
                                            <div class="swiper-image__date swiper-image__text-inner"><?=$arItem['PROPERTIES']['DATE']['VALUE']?></div>
                                        <?endif?>
                                        <?if (!empty($arItem['PROPERTIES']['TIME']['VALUE'])):?>
                                            <div class="swiper-image__time swiper-image__text-inner"><?=$arItem['PROPERTIES']['TIME']['VALUE']?></div>
                                        <?endif?>
                                    </div>
                                </div>
                            </div>
                            <?if (!empty($arItem['PROPERTIES']['VIDEO_LINK']['VALUE'])):?>
                                <a class="show-card__play show-card__play_decor show-card__play_slider" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
                                    <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                                    </svg>
                                </a>
                            <?endif?>
                        </div>
                    </div>
                <?endforeach?>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>