<?
if (!empty($arResult['IBLOCK_SECTION_ID'])) {
    $arSection = CIBlockSection::GetList(
        ['ID' => 'ASC'],
        ['IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arResult['IBLOCK_SECTION_ID']],
        false,
        ['ID', 'NAME']
    )->GetNext();

    $arResult['SECTION_NAME'] = $arSection['NAME'];
}