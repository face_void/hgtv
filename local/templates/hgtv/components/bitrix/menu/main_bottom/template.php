<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<div class="footer-nav">
    <span class="footer-nav__column footer-nav__column_1">
        <?
        // первые 5 пунктов
        $lim = (count($arResult) < 5) ? count($arResult) : 5;
        for ($i = 0; $i < $lim; $i++):
            $item = array_shift($arResult);
            ?>
            <a class="footer-nav__link" href="<?=$item['LINK']?>"><?=$item['TEXT']?></a>
        <?endfor?>
    </span>
    <span class="socials socials_footer footer-nav__column footer-nav__column_2">
        <? $APPLICATION->IncludeFile('/include/index/social.php', false, ['SHOW_BORDER' => false]) ?>
    </span>
    <?if (!empty($arResult)):?>
        <span class="footer-nav__column footer-nav__column_3">
            <?
            // следующие два пункта
            $lim = (count($arResult) < 2) ? count($arResult) : 2;
            for ($i = 0; $i < $lim; $i++):
                $item = array_shift($arResult);
                ?>
                <a class="footer-nav__link" href="<?=$item['LINK']?>"><?=$item['TEXT']?></a>
            <?endfor?>
        </span>
    <?endif?>
    <?if (!empty($arResult)):?>
        <span class="footer-nav__column footer-nav__column_4">
            <?foreach ($arResult as $item):?>
                <a class="footer-nav__link footer-nav__link--long" href="<?=$item['LINK']?>"><?=$item['TEXT']?></a>
            <?endforeach?>
        </span>
    <?endif?>
</div>