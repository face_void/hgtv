<?
$this->setFrameMode(true);
?>
<?if (!empty($arResult['ITEMS'])):?>
    <div class="single-show__swiper page-wrap">
        <div class="container" id="home-slider">
            <div id="home-slider__left">
                <div class="swiper-container slider-hero-left">
                    <div class="swiper-wrapper">
                        <?
                        // первый элемент должен совпадать, остальные элементы должны идти в обратном порядке
                        $arFirstSlider = [$arResult['ITEMS'][0]];
                        $arFirstSlider = array_merge($arFirstSlider, array_reverse(array_slice($arResult['ITEMS'], 1)));
                        ?>
                        <?foreach ($arFirstSlider as $arItem):?>
                            <div class="swiper-slide">
                                <div class="swiper-image" data-swiper-parallax-y="30%">
                                    <div class="swiper-image__inner" style="background-image: url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>)"></div>
                                </div>
                            </div>
                        <?endforeach?>
                    </div>
                </div>
            </div>
            <div id="home-slider__right">
                <div class="swiper-container slider-hero-right">
                    <div class="swiper-wrapper">
                        <?foreach ($arFirstSlider as $arItem):?>
                            <div class="swiper-slide">
                                <div class="swiper-image" data-swiper-parallax-y="30%">
                                    <div class="swiper-image__inner" style="background-image: url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>)"></div>
                                </div>
                            </div>
                        <?endforeach?>
                    </div>
                </div>
            </div>
            <div class="swiper-container slider-hero">
                <div class="swiper-wrapper">
                    <?foreach ($arResult['ITEMS'] as $arItem):

                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));
                        ?>
                        <div class="swiper-slide" id="<?=$this->GetEditAreaID($arItem['ID'])?>">
                            <div class="swiper-image" data-swiper-parallax-y="35%">
                                <div class="swiper-image__mob-bg" style="background-image: url(<?=CFile::GetPath($arItem['PROPERTIES']['BANNER_PICTURE_MOB']['VALUE'])?>)"></div>
                                <div class="swiper-image__inner" style="background-image: url(<?=CFile::GetPath($arItem['PROPERTIES']['BANNER_PICTURE']['VALUE'])?>)">
                                    <div class="swiper-image__bot">
                                        <div class="swiper-image__bot-text">
                                            <h2><?=$arItem['NAME']?></h2>
                                            <p><?=$arItem['PROPERTIES']['BANNER_TEXT']['VALUE']?></p>
                                        </div>
                                    </div>
                                </div>
                                <?if (!empty($arItem['PROPERTIES']['VIDEO_LINK']['VALUE'])):?>
                                    <a class="show-card__play show-card__play_decor show-card__play_slider" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity><svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                                        </svg>
                                    </a>
                                <?endif?>
                            </div>
                        </div>
                    <?endforeach?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
<?endif?>