<?
foreach ($arResult['ITEMS'] as &$arItem) {
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));

    $arItem['EDIT_ID'] = $this->GetEditAreaID($arItem['ID']);
}