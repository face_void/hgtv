<?
$rsSections = CIBlockSection::GetList(
    ['SORT' => 'ASC', 'ID' => 'ASC'],
    ['ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'SECTION_ID' => $arParams['PARENT_SECTION'], 'IBLOCK_ID' => $arParams['IBLOCK_ID']],
    true,
    ['ID', 'NAME']
);

while ($arSection = $rsSections->GetNext()) {
    if ($arSection['ELEMENT_CNT'] > 0) {
        $arResult['SECTIONS'][ $arSection['ID'] ] = $arSection;
    }
}

foreach ($arResult['ITEMS'] as $arItem) {
    if (isset($arResult['SECTIONS'][ $arItem['IBLOCK_SECTION_ID'] ])) {
        $arResult['SECTIONS'][ $arItem['IBLOCK_SECTION_ID'] ]['ITEMS'][] = $arItem;
    }
}