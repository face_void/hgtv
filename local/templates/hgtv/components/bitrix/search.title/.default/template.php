<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?
$INPUT_ID = trim($arParams["~INPUT_ID"]);
if(strlen($INPUT_ID) <= 0)
	$INPUT_ID = "title-search-input";
$INPUT_ID = CUtil::JSEscape($INPUT_ID);

$CONTAINER_ID = trim($arParams["~CONTAINER_ID"]);
if(strlen($CONTAINER_ID) <= 0)
	$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

if($arParams["SHOW_INPUT"] !== "N"):?>
	<div class="header__search search" id="<?echo $CONTAINER_ID?>">
        <form class="search__form" action="<?echo $arResult["FORM_ACTION"]?>">
            <button class="search__icon" type="submit">
                <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g opacity="0.4">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M16.9001 25.4C12.2057 25.4 8.40009 21.5944 8.40009 16.9C8.40009 12.2056 12.2057 8.40002 16.9001 8.40002C21.5945 8.40002 25.4001 12.2056 25.4001 16.9C25.4001 18.8869 24.7184 20.7146 23.5761 22.1619L28.5071 27.0929C28.8976 27.4834 28.8976 28.1166 28.5071 28.5071C28.1166 28.8976 27.4834 28.8976 27.0929 28.5071L22.1618 23.5761C20.7146 24.7183 18.8869 25.4 16.9001 25.4ZM16.9 23.4C20.4898 23.4 23.4 20.4899 23.4 16.9C23.4 13.3102 20.4898 10.4 16.9 10.4C13.3101 10.4 10.4 13.3102 10.4 16.9C10.4 20.4899 13.3101 23.4 16.9 23.4Z" fill="black"/>
                    </g>
                </svg>
            </button>
            <input class="search__input" id="<?echo $INPUT_ID?>" type="text" name="q" value="" autocomplete="off" placeholder="Поиск" />
        </form>
	</div>
<?endif?>
<script>
	BX.ready(function(){
		new JCTitleSearch({
			'AJAX_PAGE' : '<?echo CUtil::JSEscape(POST_FORM_ACTION_URI)?>',
			'CONTAINER_ID': '<?echo $CONTAINER_ID?>',
			'INPUT_ID': '<?echo $INPUT_ID?>',
			'MIN_QUERY_LEN': 2
		});
	});
</script>
