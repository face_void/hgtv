<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<?if (!empty($arResult['ITEMS'])):?>
    <div class="doit">
        <div class="doit__container container container_inner">
            <h2 class="doit__title title title_h1"><?$APPLICATION->IncludeFile('/include/interesting/title_diy.php', false, ['NAME' => 'Заголовок', 'MODE' => 'text'])?></h2>
            <div class="tabs">
                <div class="tabs__nav">
                    <?
                    $first = true;
                    foreach ($arResult['SECTIONS'] as $arSection):?>
                        <a class="tabs__link<?if ($first):?> active<?endif?>" href="#tab<?=$arSection['ID']?>"><?=$arSection['NAME']?></a>
                        <?
                        if ($first) $first = false;
                    endforeach;?>
                </div>
                <div class="tabs__content">
                    <?
                    $first = true;
                    foreach ($arResult['SECTIONS'] as $arSection):?>
                        <div class="tabs__pane<?if ($first):?> show<?endif?>" id="tab<?=$arSection['ID']?>">
                            <div class="swiper-container js-doit-slider">
                                <div class="swiper-wrapper">
                                    <?foreach ($arSection['ITEMS'] as $arItem):

                                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
                                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));?>
                                        <div class="swiper-slide" id="<?=$this->GetEditAreaID($arItem['ID'])?>">
                                            <div class="show-card show-card_1">
                                                <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                                    <div class="show-card__cover show-card__cover_tall">
                                                        <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                                                    </div>
                                                    <div class="show-card__info">
                                                        <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    <?endforeach?>
                                </div>
                            </div>
                        </div>
                        <?
                        if ($first) $first = false;
                    endforeach;?>
                </div>
            </div>
        </div>
    </div>
<?endif?>