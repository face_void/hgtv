<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult['CATEGORIES'])):?>
    <div class="search-suggest">
        <?foreach ($arResult['CATEGORIES'] as $category):?>
            <?foreach ($category['ITEMS'] as $arItem):?>
                <a class="search-suggest__item" href="<?=$arItem['URL']?>"><?=$arItem['NAME']?></a>
            <?endforeach?>
        <?endforeach;?>
    </div>
<?endif?>
