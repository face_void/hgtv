<?
$curDay = date( $arResult['DAY_FORMAT'] );
$curTime = date( $arResult['TIME_FORMAT'] );

foreach ($arResult['SCHEDULE'] as $day => $schedule) {
    foreach ($schedule as $k => $broadcast) {
        if ($day == $curDay) {
            // шоу за текущий день, которые уже закончились
            if ($broadcast['TIME_END'] < $curTime) {
                $arResult['SCHEDULE'][$day][$k]['PASSED'] = 'Y';
                continue;
            }

            // в эфире
            if ($broadcast['TIME_START'] < $curTime && $broadcast['TIME_END'] > $curTime) {
                $arResult['SCHEDULE'][$day][$k]['ON_AIR'] = 'Y';

                // два предыдущих шоу нужно показать в программе
                $arResult['SCHEDULE'][ $day ][ $k - 1 ]['PASSED_SHOW'] = $arResult['SCHEDULE'][ $day ][ $k - 2 ]['PASSED_SHOW'] = 'Y';
                $arResult['ON_AIR'] = $broadcast;
                break;
            }
        }
    }
}