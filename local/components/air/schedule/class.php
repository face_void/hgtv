<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class AirScheduleComponent extends CBitrixComponent
{
    public function executeComponent()
    {
        if (!\Bitrix\Main\Loader::includeModule('air.site')) {
            ShowError('Модуль air site не установлен');
            return false;
        }

        $schedule = Air\Site\Schedule::getScheduleArray();

        if (!$schedule || empty($schedule)) {
            $arResult['ERRORS'][] = "Ошибка при загрузке телепрограммы";
        } else {
            $arResult = [
                'DAY_FORMAT' => 'd.m',
                'TIME_FORMAT' => 'H:i'
            ];
            foreach ($schedule as $broadcast) {
                $startTime = new DateTime($broadcast['TIME_START']);
                $endTime = new DateTime($broadcast['TIME_END']);

                $arResult['SCHEDULE'][$startTime->format('d.m')][] = [
                    'SHOW_ID' => $broadcast['SHOW_ID'],
                    'NAME' => $broadcast['NAME'],
                    'TIME_START' => $startTime->format('H:i'),
                    'TIME_END' => $endTime->format('H:i'),
                    'SEASON' => $broadcast['SEASON'],
                    'EPISODE' => $broadcast['EPISODE'],
                    'PREVIEW_TEXT' => $broadcast['PREVIEW_TEXT']
                ];
            }
        }

        $this->arResult = $arResult;

        $this->includeComponentTemplate();

    }
}