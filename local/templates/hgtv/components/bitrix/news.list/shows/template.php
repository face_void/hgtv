<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);?>
<div class="shows-block">
    <div class="letters-line">
        <div class="letters-line__letters-container">
            <div class="letters-line__letters letters">
                <div class="container container_inner">
                    <ul class="letters__list">
                        <li class="letters__item"><a class="letters__link" href="#N">#</a></li>
                        <?foreach ($arResult['SHOWS'] as $letter => $arShows):?>
                            <li class="letters__item<?if (empty($arShows)):?> disabled<?endif?>"><a class="letters__link" href="#<?=$letter?>"><?=$letter?></a></li>
                        <?endforeach?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container container_inner">
        <div class="shows-block__list">
            <div class="shows-block__one-letter-items" id="N"></div>
            <?foreach ($arResult['SHOWS'] as $letter => $arShows):
                if (empty($arShows)) continue;
                ?>
                <div class="shows-block__one-letter-items" id="<?=$letter?>">
                    <div class="shows-block__letter"><?=$letter?></div>
                    <div class="one-letter-shows">
                        <div class="one-letter-shows__list">
                            <?foreach ($arShows as $arItem):

                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));
                                ?>
                                <div class="one-letter-shows__item" id="<?=$this->GetEditAreaID($arItem['ID'])?>">
                                    <div class="show-card">
                                        <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                            <div class="show-card__cover">
                                                <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                                            </div>
                                            <div class="show-card__info">
                                                <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?endforeach?>
                        </div>
                    </div>
                </div>
            <?endforeach?>
        </div>
    </div>
</div>