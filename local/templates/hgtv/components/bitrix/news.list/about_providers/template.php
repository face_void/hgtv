<?
$this->setFrameMode(true);
?>
<?if (!empty($arResult['ITEMS'])):?>
    <div class="about-promo__providers">
        <?foreach ($arResult['ITEMS'] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));

            $picture = (!empty($arItem['PROPERTIES']['ABOUT_PICTURE']['VALUE'])) ?
                CFile::GetPath($arItem['PROPERTIES']['ABOUT_PICTURE']['VALUE']) :
                $arItem['PREVIEW_PICTURE']['SRC'];

            if (empty($picture))
                continue;
            ?>
            <a id="<?=$this->GetEditAreaID($arItem['ID'])?>" class="about-promo__provider" href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">
                <img src="<?=$picture?>">
            </a>
        <?endforeach?>
    </div>
<?endif?>