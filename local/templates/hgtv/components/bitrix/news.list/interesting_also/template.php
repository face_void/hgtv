<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<?if (!empty($arResult['ITEMS'])):?>
    <div class="shows shows--bordered">
        <div class="shows__container container container_inner">
            <div class="shows__heading">
                <h2 class="page-subtitle"><?$APPLICATION->IncludeFile('/include/interesting/title_also.php', false, ['NAME' => 'Заголовок', 'MODE' => 'text'])?></h2>
            </div>
            <div class="shows__wrapper shows__wrapper--full">
                <?$arItem = $arResult['ITEMS'][0]?>
                <div class="shows__column shows__column--md" id="<?=$arItem['EDIT_ID']?>">
                    <div class="show-card show-card_1">
                        <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                            <div class="show-card__cover show-card__cover_tall">
                                <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>"></div>
                            <div class="show-card__info">
                                <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                            </div>
                        </a>
                    </div>
                </div>
                <?
                if (count($arResult['ITEMS']) > 1):
                    $arItem = $arResult['ITEMS'][1];?>
                    <div class="shows__column shows__column--md" id="<?=$arItem['EDIT_ID']?>">
                        <div class="show-card show-card_3">
                            <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                <div class="show-card__cover show-card__cover_tall-2">
                                    <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>"></div>
                                <div class="show-card__info">
                                    <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?endif;

                if (count($arResult['ITEMS']) > 2):
                    $arItem = $arResult['ITEMS'][2];?>
                    <div class="shows__column shows__column--lg" id="<?=$arItem['EDIT_ID']?>">
                        <div class="show-card show-card_5">
                            <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                <div class="show-card__cover show-card__cover_wide">
                                    <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>"></div>
                                <div class="show-card__info">
                                    <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?endif?>
            </div>
        </div>
    </div>
<?endif?>