<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="search-results">
    <div class="container container_inner">
        <h1 class="search-results__title">“<?=$arResult['REQUEST']['~QUERY']?>”</h1>
        <div class="search-results__form">
            <form class="search__form" action="" method="get" autocomplete="off">
                <button class="search__icon" type="submit">
                    <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g opacity="0.4">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M16.9001 25.4C12.2057 25.4 8.40009 21.5944 8.40009 16.9C8.40009 12.2056 12.2057 8.40002 16.9001 8.40002C21.5945 8.40002 25.4001 12.2056 25.4001 16.9C25.4001 18.8869 24.7184 20.7146 23.5761 22.1619L28.5071 27.0929C28.8976 27.4834 28.8976 28.1166 28.5071 28.5071C28.1166 28.8976 27.4834 28.8976 27.0929 28.5071L22.1618 23.5761C20.7146 24.7183 18.8869 25.4 16.9001 25.4ZM16.9 23.4C20.4898 23.4 23.4 20.4899 23.4 16.9C23.4 13.3102 20.4898 10.4 16.9 10.4C13.3101 10.4 10.4 13.3102 10.4 16.9C10.4 20.4899 13.3101 23.4 16.9 23.4Z" fill="black"/>
                        </g>
                    </svg>
                </button>
                <input class="search__input" type="text" name="q" placeholder="Поиск">
            </form>
        </div>
        <?if (empty($arResult['SEARCH'])):?>
            <div class="tabs">
                <div class="tabs__nav">
                    <div class="search-results__empty"><span>0</span> результатов</div>
                </div>
                <div class="tabs__content">
                    <div class="search-results__text">
                        <p>Ваш запрос не дал результатов. Попробуйте:</p>
                        <ul>
                            <li>Другое ключевое слово</li>
                            <li>Меньше ключевых слов</li>
                            <li>Проверить, правильно ли написаны поисковые слова</li>
                        </ul>
                    </div>
                </div>
            </div>
        <?else:?>
            <div class="tabs">
                <div class="tabs__nav">
                    <?
                    $first = true;
                    foreach ($arResult['SEARCH_RESULT'] as $id => $cat):
                        if (empty($cat['ITEMS'])) continue;
                        ?>
                        <a class="tabs__link<?if ($first):?> active<?endif?>" href="#cat<?=$id?>"><?=$cat['TITLE']?> (<?=count($cat['ITEMS'])?>)</a>
                        <?
                        $first = false;
                    endforeach;?>
                </div>
                <div class="tabs__content">
                    <?
                    $first = true;
                    foreach ($arResult['SEARCH_RESULT'] as $id => $cat):
                        if (empty($cat['ITEMS'])) continue;

                        // Видео
                        if ($id == '2'):?>
                            <div class="tabs__pane<?if ($first):?> show<?endif?>" id="cat<?=$id?>">
                                <div class="tabs__video">
                                    <?foreach ($cat['ITEMS'] as $arItem):?>
                                        <div class="video__card show-card">
                                            <a class="show-card__link" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
                                                <div class="show-card__cover">
                                                    <img class="show-card__pic" src="<?=CFile::GetPath($arItem['PREVIEW_PICTURE'])?>">
                                                    <div class="show-card__play">
                                                        <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                                                        </svg>
                                                    </div>
                                                </div>
                                                <div class="show-card__info">
                                                    <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                                                    <div class="show-card__time title title_h4"><?=$arItem['PROPERTIES']['VIDEO_LENGTH']['VALUE']?></div>
                                                </div>
                                            </a>
                                        </div>
                                    <?endforeach?>
                                </div>
                            </div>
                            <?
                            $first = false;

                        // Шоу
                        elseif ($id == '3'):?>
                            <div class="tabs__pane<?if ($first):?> show<?endif?>" id="cat<?=$id?>">
                                <div class="tabs__shows">
                                    <?foreach ($cat['ITEMS'] as $arItem):?>
                                        <div class="tabs__show">
                                            <div class="show-card show-card_1">
                                                <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                                    <div class="show-card__cover show-card__cover_tall">
                                                        <img class="show-card__pic" src="<?=CFile::GetPath($arItem['PREVIEW_PICTURE'])?>">
                                                    </div>
                                                    <div class="show-card__info">
                                                        <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    <?endforeach?>
                                </div>
                            </div>
                            <?
                            $first = false;

                        // Статьи
                        elseif ($id == '8'):?>
                            <div class="tabs__pane<?if ($first):?> show<?endif?>" id="cat<?=$id?>">
                                <?foreach ($cat['ITEMS'] as $arItem):?>
                                    <div class="tabs__show">
                                        <div class="show-card show-card_1">
                                            <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                                <div class="show-card__cover show-card__cover_tall">
                                                    <img class="show-card__pic" src="<?=CFile::GetPath($arItem['PREVIEW_PICTURE'])?>">
                                                </div>
                                                <div class="show-card__info">
                                                    <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <?endforeach?>
                            </div>
                            <?
                            $first = false;

                        endif?>
                    <?endforeach?>
                </div>
            </div>
        <?endif?>
    </div>
</div>
