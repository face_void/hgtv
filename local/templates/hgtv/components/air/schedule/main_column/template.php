<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>

<div class="video__card video__card_timetable timetable">
    <div class="video__on-air-heading">Сейчас в эфире</div>
    <div class="timetable__container">
        <div class="timetable__top">
            <?if (!empty($arResult['ERRORS'])):?>
                <?foreach ($arResult['ERRORS'] as $error):?>
                    <p class="error"><?=$error?></p>
                <?endforeach?>
            <?else:?>
                <?foreach ($arResult['SCHEDULE'] as $broadcast):?>
                    <div class="timetable__line">
                        <div class="timetable__heading"><?=$broadcast['NAME']?></div>
                        <div class="timetable__info">
                            <div class="timetable__season">Сезон <?=$broadcast['SEASON']?>, Серия <?=$broadcast['EPISODE']?></div>
                            <div class="timetable__time"><?=$broadcast['TIME_START']?></div>
                        </div>
                    </div>
                <?endforeach?>
            <?endif?>
        </div>
        <a class="timetable__btn" href="/schedule/">все расписания</a>
    </div>
</div>