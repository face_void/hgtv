<?php
namespace Air\Site;

use Bitrix\Highloadblock\HighloadBlockTable;

/**
 * Класс для работы с highload блоками. Содержит методы для получения данных из HL блока.
 *
 */
class HL
{
    /**
     * Получить объект базы по имени таблицы
     *
     * @param string $sTable Имя таблицы
     * @return \Bitrix\Main\Entity\Base
     * @throws \Bitrix\Main\SystemException
     */
    public static function getEntityClassByTableName($sTable)
    {
        static $arClasses = [];

        if (!array_key_exists($sTable, $arClasses)) {
            $arClasses[$sTable] = '';

            $arHLData = HighloadBlockTable::getRow([
                'filter' => ['TABLE_NAME' => $sTable]
            ]);

            if ($arHLData) {
                $arClasses[$sTable] = HighloadBlockTable::compileEntity($arHLData);
            }
        }

        return $arClasses[$sTable];
    }

    /**
     * Получить объект базы по имени highload блока
     *
     * @param string $sName Имя блока
     * @return \Bitrix\Main\Entity\Base
     * @throws \Bitrix\Main\SystemException
     */
    public static function getEntityClassByHLName($sName)
    {
        static $arClasses = [];

        if (!array_key_exists($sName, $arClasses)) {
            $arClasses[$sName] = '';

            $arHLData = HighloadBlockTable::getRow([
                'filter' => ['NAME' => $sName]
            ]);

            if ($arHLData) {
                $arClasses[$sName] = HighloadBlockTable::compileEntity($arHLData);
            }
        }

        return $arClasses[$sName];
    }

    public static function getElements($id)
    {
        $hlblock = HighloadBlockTable::getById($id)->fetch();
        $entity = HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();

        $res = $entityClass::getList(array(
            'select' => array('*'),
            'order' => array('ID' => 'ASC'),
        ));

        return $res;
    }

    public static function getElementsByTableName($sTable)
    {
        $entity = static::getEntityClassByTableName($sTable);
        $entityClass = $entity->getDataClass();

        $res = $entityClass::getList(array(
            'select' => array('*'),
            'order' => array('ID' => 'ASC'),
        ));

        return $res;
    }

    /**
     * Возвращает значение поля по свойству
     *
     * @param array $arProp Свойство
     * @param string $sField Поле
     * @param bool $multiple
     * @return string|array
     * @throws \Bitrix\Main\SystemException
     */
    public static function getFieldValueByProp($arProp, $sField, $multiple = false)
    {
        if ($arProp['PROPERTY_TYPE'] != 'S')
            return 'Property type is not S';

        $sTableName = $arProp['USER_TYPE_SETTINGS']['TABLE_NAME'];
        $obEntity = self::getEntityClassByTableName($sTableName);

        if (!$obEntity || !is_object($obEntity))
            return 'Failed to load entity';

        if (!$obEntity->hasField('UF_XML_ID') || !$obEntity->hasField($sField))
            return "No such field \"$sField\" in table";

        $sClass = $obEntity->getDataClass();

        if ($multiple) {
            $arPropValue = [];

            if (!is_array($arProp['~VALUE']))
                $arProp['~VALUE'] = [$arProp['~VALUE']];

            foreach ($arProp['~VALUE'] as $sVal) {
                $arPropValue[] = $sClass::getRow([
                    'filter' => ['UF_XML_ID' => $sVal]
                ]);
            }

            return $arPropValue;
        }

        $arPropValue = $sClass::getRow([
            'filter' => ['UF_XML_ID' => $arProp['~VALUE']],
            'select' => [$sField]
        ]);

        return $arPropValue[$sField];
    }

    /**
     * Возвращает значение всех полей свойства
     *
     * @param array $arProp Свойство
     * @param bool $multiple
     * @return string|array
     * @throws \Bitrix\Main\SystemException
     */
    public static function getFieldsByProp($arProp)
    {
        if ($arProp['PROPERTY_TYPE'] != 'S')
            return 'Property type is not S';

        $sTableName = $arProp['USER_TYPE_SETTINGS']['TABLE_NAME'];
        $obEntity = self::getEntityClassByTableName($sTableName);

        if (!$obEntity || !is_object($obEntity))
            return 'Failed to load entity';

        $sClass = $obEntity->getDataClass();

        $arPropValue = $sClass::getList([
            'filter' => ['UF_XML_ID' => $arProp['~VALUE']],
            'select' => ['*']
        ])->fetch();

        return $arPropValue;
    }

    /**
     * Возвращает поле по свойству
     *
     * @param array $arProp Свойство
     * @return array Поле
     * @throws \Bitrix\Main\SystemException
     */
    public static function getFileByProp($arProp)
    {
        $iFile = self::getFieldValueByProp($arProp, 'UF_FILE');

        if (!$iFile)
            return [];

        $arFile = \CFile::GetFileArray($iFile);

        return $arFile;
    }
}