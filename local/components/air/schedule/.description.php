<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => 'Телепрограмма',
    "DESCRIPTION" => 'Загружает актуальную телепрограмму',
    "CACHE_PATH" => "Y",
    "SORT" => 10,
    "PATH" => array(
        "ID" => "air",
        "NAME" => 'AIR',
        "SORT" => 10000
    ),
);