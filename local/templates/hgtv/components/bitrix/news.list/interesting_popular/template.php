<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<?if (!empty($arResult['ITEMS'])):?>
    <div class="popular">
        <div class="popular__container container container_inner">
            <div class="popular__heading title title_h1"><?$APPLICATION->IncludeFile('/include/interesting/title_popular.php', false, ['NAME' => 'Заголовок', 'MODE' => 'text'])?></div>
            <?foreach ($arResult['ITEMS'] as $arItem):

                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));?>
                <div class="article-card" id="<?=$this->GetEditAreaID($arItem['ID'])?>">
                    <div class="article-card__desc">
                        <div class="article-card__date"><?=$arItem['DISPLAY_ACTIVE_FROM']?></div>
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                            <h3 class="article-card__title"><?=$arItem['NAME']?></h3>
                        </a>
                        <p class="article-card__text"><?=$arItem['PREVIEW_TEXT']?></p>
                    </div>
                    <div class="article-card__img-wrap">
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                            <div class="article-card__img" style="background-image: url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>)"></div>
                        </a>
                    </div>
                </div>
            <?endforeach?>
        </div>
    </div>
<?endif?>