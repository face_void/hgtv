<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<div class="provider__wrapper">
    <?if (empty($arResult['ITEMS'])):?>
        <div class="provider__empty-result">На данный момент для вашего города нет провайдеров</div>
    <?else:?>
        <?foreach ($arResult['ITEMS'] as $k => $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));
            ?>
            <div class="provider__card<?if (($k + 1) % 5 == 0):?> provider__card_large<?endif?>" id="<?=$this->GetEditAreaID($arItem['ID'])?>">
                <div class="provider__name"><?=$arItem['NAME']?></div>
                <div class="provider__pic"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>"></div>
                <div class="provider__bot">
                    <?if (!empty($arItem['PROPERTIES']['LINK']['VALUE'])):?>
                        <div class="provider__link"><?=$arItem['PROPERTIES']['LINK']['VALUE']?></div>
                        <div class="provider__btn btn">
                            <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" class="btn__link">перейти</a>
                        </div>
                    <?endif?>
                </div>
            </div>
        <?endforeach?>
    <?endif?>
</div>