<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
$arItems = $arResult['ITEMS'];
?>
<div class="video">
    <div class="video__container container container_inner">
        <div class="video__line video__line_top">
            <div class="video__card show-card">
                <div class="video__main-heading"><?$APPLICATION->IncludeFile('/include/index/sec_video_title.php', false, ['NAME' => 'Заголовок', 'MODE' => 'html'])?></div>
                <?
                // первый элемент
                if (!empty($arItems[0])):
                    $arItem = $arItems[0];
                    ?>
                    <a id="<?=$arItem['EDIT_ID']?>" class="show-card__link" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
                        <div class="show-card__cover">
                            <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                            <div class="show-card__play">
                                <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                                </svg>
                            </div>
                        </div>
                        <div class="show-card__info">
                            <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                            <div class="show-card__time title title_h4"><?=$arItem['PROPERTIES']['VIDEO_LENGTH']['VALUE']?></div>
                        </div>
                    </a>
                <?endif?>
            </div>
            <div class="video__card show-card">
                <div class="video__main-heading-mob"><?$APPLICATION->IncludeFile('/include/index/sec_video_title.php', false, ['NAME' => 'Заголовок', 'MODE' => 'html'])?></div>
                <?
                // второй элемент
                if (!empty($arItems[1])):
                    $arItem = $arItems[1];
                    ?>
                    <a id="<?=$arItem['EDIT_ID']?>" class="show-card__link" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
                        <div class="show-card__cover">
                            <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                            <div class="show-card__play"><svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                                </svg>
                            </div>
                        </div>
                        <div class="show-card__info">
                            <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                            <div class="show-card__time title title_h4"><?=$arItem['PROPERTIES']['VIDEO_LENGTH']['VALUE']?></div>
                        </div>
                    </a>
                <?endif?>
            </div>
            <?$APPLICATION->IncludeComponent(
                "air:schedule",
                "main_column",
                Array(
                )
            );?>
        </div>
        <div class="video__line">
            <div class="video__card show-card">
                <?
                // третий элемент
                if (!empty($arItems[2])):
                    $arItem = $arItems[2];
                    ?>
                    <a id="<?=$arItem['EDIT_ID']?>" class="show-card__link" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
                        <div class="show-card__cover">
                            <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                            <div class="show-card__play">
                                <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                                </svg>
                            </div>
                        </div>
                        <div class="show-card__info">
                            <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                            <div class="show-card__time title title_h4"><?=$arItem['PROPERTIES']['VIDEO_LENGTH']['VALUE']?></div>
                        </div>
                    </a>
                <?endif?>
            </div>
            <div class="video__card show-card">
                <?
                // четвёртый элемент
                if (!empty($arItems[3])):
                    $arItem = $arItems[3];
                    ?>
                    <a id="<?=$arItem['EDIT_ID']?>" class="show-card__link" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
                        <div class="show-card__cover">
                            <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                            <div class="show-card__play">
                                <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                                </svg>
                            </div>
                        </div>
                        <div class="show-card__info">
                            <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                            <div class="show-card__time title title_h4"><?=$arItem['PROPERTIES']['VIDEO_LENGTH']['VALUE']?></div>
                        </div>
                    </a>
                <?endif?>
            </div>
            <div class="video__card video__card_person show-card show-card">
                <div class="show-card__link">
                    <div class="show-card__cover">
                        <img class="show-card__pic" src="<?=SITE_TEMPLATE_PATH?>/assets/img/lined-bg.png">
                        <div class="show-card__circle"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/circle.png"></div>
                        <div class="show-card__photo"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/kristina-single.jpg"></div>
                    </div>
                    <div class="show-card__info">
                        <div class="show-card__name title title_h4">Кристина Анстед</div>
                        <div class="show-card__time title title_h4">Игра вслепую</div>
                        <!--.show-card__time.title.title_h4 Игра вслепую-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>