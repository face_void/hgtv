<?
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;

// получаем ID модуля и подключаем его
$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

Loader::includeModule($module_id);

// массив с вкладками и настройками
$aTabs = [
    [
        "DIV" => "social",
        "TAB" => "Соцсети",
        "TITLE" => "Соцсети",
        "OPTIONS" => [
            [
                "vk",
                "Вконтакте:",
                "",
                ["text", 20]
            ],
            [
                "inst",
                "Instagram:",
                "",
                ["text", 20]
            ],
            [
                "ok",
                "Одноклассники:",
                "",
                ["text", 20]
            ],
        ]
    ],
    [
        "DIV" => "schedule",
        "TAB" => "Телепрограмма",
        "TITLE" => "Телепрограмма",
        "OPTIONS" => [
            [
                "schedule_link",
                "Ссылка на файл с телепрограммой:",
                "",
                ["text", 30]
            ],
        ]
    ]
];

// сохранение настроек
if ($request->isPost() && check_bitrix_sessid() && $request["apply"]) {
    foreach ($aTabs as $aTab) {
        foreach ($aTab["OPTIONS"] as $arOption) {
            __AdmSettingsSaveOptions($module_id, $aTab['OPTIONS']);
        }
    }

    LocalRedirect($APPLICATION->GetCurPage()."?mid=".$module_id."&lang=".LANG);
}

// вывод формы с настройками
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$tabControl->Begin();
?>
<form action="<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANG); ?>" method="post">
    <?
    echo bitrix_sessid_post();

    foreach($aTabs as $aTab) {
        if ($aTab["OPTIONS"]) {
            $tabControl->BeginNextTab();
            __AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
        }
    }

    $tabControl->Buttons();
    ?>
    <input type="submit" name="apply" value="Применить" class="adm-btn-save">
</form>
<?$tabControl->End();?>
