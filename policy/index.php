<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Положение о конфиденциальности");
?>
<div class="terms">
    <div class="container container_inner">
        <div class="terms__wrap">
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "left",
                Array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "left",
                    "USE_EXT" => "N"
                )
            );?>
            <div class="terms__content">
                <h1 class="terms__title"><?$APPLICATION->ShowTitle(false);?></h1>
                <?$APPLICATION->IncludeFile('/include/policy/policy.php', false, ['NAME' => 'Политика', 'MODE' => 'html'])?>
                <div class="terms__bottom"><span>Последнее обновление</span><span class="terms__date"><?$APPLICATION->IncludeFile('/include/policy/policy_date.php', false, ['NAME' => 'Дата', 'MODE' => 'text'])?></span></div>
            </div>
        </div>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>