<div class="article">
    <div class="show-promo">
        <div class="show-promo__wrapper" style="background-image: url('<?=$arResult['DETAIL_PICTURE']['SRC']?>')">
            <div class="show-promo__container container container_inner">
                <div id="whiteHeader"></div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb",
                    "white_shows_detail",
                    Array(
                        "PATH" => "",
                        "SITE_ID" => "s1",
                        "START_FROM" => "0",
                    )
                );?>
                <div class="show-promo__content">
                    <?if (!empty($arResult['SECTION_NAME'])):?>
                        <div class="show-promo__tag"><?=$arResult['SECTION_NAME']?></div>
                    <?endif?>
                    <h1 class="show-promo__title"><?=$arResult['NAME']?></h1>
                    <?if (!empty($arResult['PROPERTIES']['SUBTITLE']['VALUE'])):?>
                        <div class="show-promo__text"><?=$arResult['PROPERTIES']['SUBTITLE']['VALUE']?></div>
                    <?endif?>
                </div>
            </div>
        </div>
    </div>
    <div class="article__container container container_inner">
        <div class="article__socials">
            <a class="article__social" href="https://www.facebook.com/sharer/sharer.php?u=https://<?=urlencode($_SERVER['HTTP_HOST'].$APPLICATION->GetCurPage())?>">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.95937 13.0422V19.9H12.8984V13.0422H14.8578L15.4142 10.6185H12.8984V8.96282C12.8984 8.28488 13.2687 7.55403 14.2425 7.55403H15.4152V5.00196L13.6489 5C11.1467 5 9.95937 6.48912 9.95937 8.60915V10.6175H8V13.0422H9.95937Z" fill="black"/>
                </svg>
            </a>
            <a class="article__social" href="https://vk.com/share.php?url=https://<?=urlencode($_SERVER['HTTP_HOST'].$APPLICATION->GetCurPage())?>">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M15.8043 19.9582C15.8043 19.9582 16.1266 19.9231 16.2917 19.749C16.4428 19.5896 16.4375 19.2886 16.4375 19.2886C16.4375 19.2886 16.4174 17.8835 17.0821 17.676C17.7371 17.472 18.5782 19.0349 19.4708 19.6359C20.1451 20.0902 20.6569 19.9908 20.6569 19.9908L23.0421 19.9582C23.0421 19.9582 24.2893 19.8828 23.698 18.92C23.6491 18.8411 23.3531 18.2076 21.9251 16.9061C20.4289 15.5439 20.6298 15.7642 22.4307 13.4074C23.5277 11.9723 23.9662 11.0961 23.8291 10.7214C23.6989 10.3631 22.8919 10.4582 22.8919 10.4582L20.2071 10.4745C20.2071 10.4745 20.008 10.448 19.8604 10.5345C19.7162 10.6194 19.6228 10.8175 19.6228 10.8175C19.6228 10.8175 19.1983 11.9277 18.6315 12.8725C17.4358 14.8649 16.9581 14.9703 16.7624 14.8469C16.3074 14.5579 16.4209 13.6878 16.4209 13.0696C16.4209 11.1381 16.7196 10.3331 15.8401 10.1247C15.5484 10.0553 15.3336 10.0099 14.5868 10.0021C13.6287 9.99271 12.8182 10.0056 12.3588 10.2259C12.0531 10.3725 11.8173 10.7 11.9614 10.7189C12.1387 10.742 12.5404 10.8252 12.7535 11.1098C13.0287 11.4776 13.0191 12.3023 13.0191 12.3023C13.0191 12.3023 13.1771 14.5759 12.6496 14.858C12.288 15.0518 11.7919 14.6565 10.7255 12.8484C10.1797 11.9225 9.76742 10.8989 9.76742 10.8989C9.76742 10.8989 9.68794 10.7077 9.54557 10.6048C9.37352 10.4805 9.13333 10.442 9.13333 10.442L6.58215 10.4582C6.58215 10.4582 6.19873 10.4685 6.05811 10.6323C5.93322 10.7772 6.04851 11.0781 6.04851 11.0781C6.04851 11.0781 8.04596 15.6656 10.308 17.9778C12.3824 20.0971 14.737 19.9582 14.737 19.9582H15.8043Z" fill="black"/>
                </svg>
            </a>
            <a class="article__social" href="#">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.5931 9.86221L14.2703 11.9519L10.5931 14.0416V9.86221ZM19.06 11.9558C19.06 11.9558 19.06 9.65363 18.7664 8.54892C18.6042 7.93862 18.1291 7.45965 17.5227 7.29742C16.4295 7 12.03 7 12.03 7C12.03 7 7.63046 7 6.53733 7.29742C5.9309 7.45965 5.45579 7.93862 5.29356 8.54892C5 9.64977 5 11.9558 5 11.9558C5 11.9558 5 14.2579 5.29356 15.3626C5.45579 15.9729 5.9309 16.4326 6.53733 16.5948C7.63046 16.8884 12.03 16.8884 12.03 16.8884C12.03 16.8884 16.4295 16.8883 17.5227 16.5909C18.1291 16.4287 18.6042 15.969 18.7664 15.3587C19.06 14.2579 19.06 11.9558 19.06 11.9558Z" fill="black"/>
                </svg>
            </a>
        </div>
        <?if ($arResult['PROPERTIES']['NAV_ENABLE']['VALUE'] && !empty($arResult['PROPERTIES']['NAV_LIST']['VALUE'])):?>
            <div class="article-sidebar" data-nav-wrap>
                <ul class="article-sidebar__nav" data-nav>
                    <?
                    $first = true;
                    foreach ($arResult['PROPERTIES']['NAV_LIST']['VALUE'] as $k => $title):?>
                        <li class="article-sidebar__item<?if ($first):?> active<?endif?>"><a href="#section-<?=$k?>" data-link="<?=$k?>"><?=$title?></a></li>
                        <?
                        if ($first) $first = false;
                    endforeach?>
                </ul>
            </div>
        <?endif?>
        <div class="article__main-content"><?=$arResult['DETAIL_TEXT']?></div>
    </div>
</div>