$(document).ready(function () {
    if ($('.article-sidebar').length) {
        $('.article__block').each(function (i, e) {
            $(e).attr('data-active', i).attr('id', 'section-'+i);
        });
    }

    // sharing
    $('.article__social').click(function (e) {
        e.preventDefault();
        var link = $(this).attr('href');

        if (link == '#')
            return false;

        window.open(
            link,
            'blank',
            'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600'
        );
    })
})