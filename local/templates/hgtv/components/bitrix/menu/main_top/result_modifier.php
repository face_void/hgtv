<?
// дополнительная проверка на выбранный пункт меню
foreach ($arResult as &$item) {
    if ($item['SELECTED']) {

        // находим текущий конечный элемент из url
        $curUrl = $APPLICATION->GetCurPage();
        $arUrl = explode('/', $curUrl);
        $curItem = $arUrl[ count($arUrl) - 2 ];

        // если отличается от ссылки в меню - значит мы не в данном разделе, а глубже
        $item['SELECTED'] = trim($item['LINK'], '/') == $curItem;
    }
}