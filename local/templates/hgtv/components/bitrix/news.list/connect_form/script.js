$(document).ready(function () {
    $(document).on('submit', '[name=connect_form]', function (e) {
        e.preventDefault();

        var form = $(this),
            formData = new FormData(form[0]);

        formData.append('ajax_call', 'Y');

        $.ajax({
            url: form.attr('action'),
            data: formData,
            dataType: 'html',
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {
                $('.provider__container').html(result);
            }
        });
    });
});