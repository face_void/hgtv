<?
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;

class air_site extends CModule
{
    public function __construct()
    {
        if(file_exists(__DIR__."/version.php")) {

            $arModuleVersion = [];

            include_once(__DIR__."/version.php");

            $this->MODULE_ID = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = "Настройки сайта";
            $this->MODULE_DESCRIPTION = "Модуль с настройками, используемыми на сайте";
            $this->PARTNER_NAME = "Air Agency";
            $this->PARTNER_URI = "https://air.agency";
        }

        return false;
    }

    public function DoInstall()
    {
        global $APPLICATION;

        if (CheckVersion(ModuleManager::getVersion("main"), "14.00.00")) {
            // @todo написать установку
            //$this->InstallFiles();
            $this->InstallDB();

            ModuleManager::registerModule($this->MODULE_ID);

            //$this->InstallEvents();
        } else {
            $APPLICATION->ThrowException("Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.");
        }

        $APPLICATION->IncludeAdminFile("Установка модуля \"Сайт\"", __DIR__."/step.php");

        return false;
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallDb();

        ModuleManager::unRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile("Удаление модуля \"Сайт\"", __DIR__."/unstep.php");
    }

    public function InstallDB()
    {
        return false;
    }

    public function UnInstallDb()
    {
        Option::delete($this->MODULE_ID);

        return false;
    }
}