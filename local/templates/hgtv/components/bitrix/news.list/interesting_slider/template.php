<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<?if (!empty($arResult['ITEMS'])):?>
    <div class="swiper-container js-promo-slider">
        <div class="swiper-wrapper">
            <?foreach ($arResult['ITEMS'] as $arItem):

                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));
                ?>
                <div class="swiper-slide" id="<?=$this->GetEditAreaID($arItem['ID'])?>">
                    <div class="swiper-image">
                        <div class="swiper-image__mob-bg" style="background-image: url(<?=CFile::GetPath($arItem['PROPERTIES']['SLIDER_PICTURE_MOBILE']['VALUE'])?>)"></div>
                        <div class="swiper-image__inner" style="background-image: url(<?=CFile::GetPath($arItem['PROPERTIES']['SLIDER_PICTURE']['VALUE'])?>)">
                            <div class="swiper-image__bot">
                                <div class="container">
                                    <div class="swiper-image__wrap">
                                        <div class="swiper-image__bot-text">
                                            <h2><?=$arItem['NAME']?></h2>
                                        </div>
                                        <a class="btn" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                            <div class="btn__link">подробнее</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?endforeach?>
        </div>
        <div class="promo-slider__controls">
            <div class="swiper-button-prev promo-slider__btn-prev">
                <svg width="43" height="43" viewBox="0 0 43 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M2.93547 21.8492H41.4618L41.4618 20.2487L3.22794 20.2487L12.3116 11.165L11.1799 10.0333L-0.000102045 21.2133L1.13163 22.345L1.14971 22.3269L11.5206 32.6979L12.6524 31.5661L2.93547 21.8492Z" fill="white"/>
                </svg>
            </div>
            <div class="swiper-button-next promo-slider__btn-next">
                <svg width="43" height="43" viewBox="0 0 43 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M39.3263 21.8492L0.79991 21.8492L0.799911 20.2487H39.0338L29.9501 11.165L31.0818 10.0333L42.2618 21.2133L41.1301 22.345L41.112 22.3269L30.7411 32.6979L29.6094 31.5661L39.3263 21.8492Z" fill="white"/>
                </svg>
            </div>
        </div>
    </div>
<?endif?>
