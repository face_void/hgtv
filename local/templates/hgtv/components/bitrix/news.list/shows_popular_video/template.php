<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
$arItem = $arResult['ITEMS'][0];
?>
<?if (!empty($arItem)):?>
    <div class="now-show__container">
        <div class="now-show__inner">
            <div class="now-show__video">
                <div class="show-card">
                    <a class="show-card__link" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
                        <div class="show-card__text">
                            <div class="now-show__text">Сейчас популярно:</div>
                            <div class="now-show__show-name"><?=$arItem['NAME']?></div>
                        </div>
                        <div class="show-card__cover show-card__cover_all-shows">
                            <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                            <div class="show-card__play show-card__play_decor show-card__play_all-show">
                                <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                                </svg>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
<?endif;?>
