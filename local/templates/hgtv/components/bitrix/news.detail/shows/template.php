<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<div class="single-show">
    <div class="show-promo">
        <div class="show-promo__wrapper" style="background-image: url('<?=$arResult['DETAIL_PICTURE']['SRC']?>')">
            <div class="show-promo__container container container_inner">
                <div id="whiteHeader"></div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb",
                    "white_shows_detail",
                    Array(
                        "PATH" => "",
                        "SITE_ID" => "s1",
                        "START_FROM" => "0",
                    )
                );?>
                <div class="show-promo__content">
                    <?if (!empty($arResult['PROPERTIES']['TYPE']['VALUE'])):?>
                        <div class="show-promo__tag"><?=$arResult['PROPERTIES']['TYPE']['VALUE']?></div>
                    <?endif?>
                    <h1 class="show-promo__title"><?=$arResult['NAME']?></h1>
                    <div class="show-promo__date"><?=$arResult['PROPERTIES']['DATE']['VALUE']?> <span><?=$arResult['PROPERTIES']['TIME']['VALUE']?></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container container_inner">
        <div class="single-show__desc"><?=$arResult['DETAIL_TEXT']?></div>
    </div>
    <?if (!empty($arResult['PROPERTIES']['TV_PRESENTER']['DETAIL'])):
        $presenter = $arResult['PROPERTIES']['TV_PRESENTER']['DETAIL'];
        ?>
        <div class="show-speaker">
            <div class="container container_inner">
                <div class="show-speaker__wrap">
                    <div class="show-speaker__info">
                        <div class="show-speaker__photo" style="background-image: url('<?=CFile::GetPath($presenter['PREVIEW_PICTURE'])?>')"></div>
                        <h3 class="show-speaker__name"><?=$presenter['NAME']?></h3><span class="show-speaker__post"><?=$presenter['PROPERTIES']['POSITION']['VALUE']?></span>
                    </div>
                    <div class="show-speaker__text">
                        <h3><?=$presenter['PREVIEW_TEXT']?></h3>
                        <p><?=$presenter['DETAIL_TEXT']?></p>
                    </div>
                </div>
            </div>
        </div>
    <?endif?>
    <?if (!empty($arResult['PROPERTIES']['SLIDER']['ITEMS'])):
        $arSlides = $arResult['PROPERTIES']['SLIDER']['ITEMS'];
        ?>
        <div class="single-show__swiper page-wrap">
            <div class="container" id="home-slider">
                <div id="home-slider__left">
                    <div class="swiper-container slider-hero-left">
                        <?
                        // первый элемент должен совпадать, остальные элементы должны идти в обратном порядке
                        $arFirstSlider = [$arSlides[0]];
                        $arFirstSlider = array_merge($arFirstSlider, array_reverse(array_slice($arSlides, 1)));
                        ?>
                        <div class="swiper-wrapper">
                            <?foreach ($arFirstSlider as $arItem):?>
                                <div class="swiper-slide">
                                    <div class="swiper-image" data-swiper-parallax-y="30%">
                                        <div class="swiper-image__inner" style="background-image: url(<?=CFile::GetPath($arItem['PREVIEW_PICTURE'])?>)"></div>
                                    </div>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
                <div id="home-slider__right">
                    <div class="swiper-container slider-hero-right">
                        <div class="swiper-wrapper">
                            <?foreach ($arFirstSlider as $arItem):?>
                                <div class="swiper-slide">
                                    <div class="swiper-image" data-swiper-parallax-y="30%">
                                        <div class="swiper-image__inner" style="background-image: url(<?=CFile::GetPath($arItem['PREVIEW_PICTURE'])?>)"></div>
                                    </div>
                                </div>
                            <?endforeach?>
                        </div>
                    </div>
                </div>
                <div class="swiper-container slider-hero">
                    <div class="swiper-wrapper">
                        <?foreach ($arSlides as $arItem):?>
                            <div class="swiper-slide">
                                <div class="swiper-image" data-swiper-parallax-y="35%">
                                    <div class="swiper-image__mob-bg" style="background-image: url(<?=CFile::GetPath($arItem['DETAIL_PICTURE'])?>)"></div>
                                    <div class="swiper-image__inner" style="background-image: url(<?=CFile::GetPath($arItem['PREVIEW_PICTURE'])?>)">
                                        <div class="swiper-image__bot">
                                            <div class="swiper-image__bot-text">
                                                <h2><?=$arItem['NAME']?></h2>
                                                <p><?=$arItem['PREVIEW_TEXT']?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?if (!empty($arItem['PROPERTIES']['VIDEO_LINK']['VALUE'])):?>
                                        <a class="show-card__play show-card__play_decor show-card__play_slider" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
                                            <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                                            </svg>
                                        </a>
                                    <?endif?>
                                </div>
                            </div>
                        <?endforeach;?>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    <?endif?>
    <?if (!empty($arResult['PROPERTIES']['TV_PRESENTER_QUOTE']['VALUE'])):?>
        <div class="single-show__quote">
            <div class="container container_inner">
                <p>“<?=$arResult['PROPERTIES']['TV_PRESENTER_QUOTE']['VALUE']?>”</p>
                <h3 class="show-speaker__name"><?=$presenter['NAME']?></h3><span class="show-speaker__post"><?=$presenter['PROPERTIES']['POSITION']['VALUE']?></span>
            </div>
        </div>
    <?endif?>
    <?if (!empty($arResult['PROPERTIES']['GALLERY']['VALUE'])):
        // только три картинки
        $arGallery = array_slice($arResult['PROPERTIES']['GALLERY']['VALUE'], 0, 3);
        $firstPic = array_shift($arGallery);
        ?>
        <div class="container container_inner">
            <div class="single-show__images">
                <div class="single-show__img single-show__img--lg">
                    <img src="<?=CFile::GetPath($firstPic)?>">
                </div>
                <?foreach ($arGallery as $imageId):?>
                    <div class="single-show__img">
                        <img src="<?=CFile::GetPath($imageId)?>">
                    </div>
                <?endforeach?>
            </div>
        </div>
    <?endif?>
    <?if (!empty($arResult['PROPERTIES']['YOU_LIKE']['ITEMS'])):?>
        <div class="recommendation">
            <div class="container container_inner">
                <h2 class="recommendation__title">Также тебе может понравиться:</h2>
                <div class="recommendation__wrap">
                    <?foreach ($arResult['PROPERTIES']['YOU_LIKE']['ITEMS'] as $arItem):?>
                        <div class="recommendation__item">
                            <div class="show-card show-card_1">
                                <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                    <div class="show-card__cover show-card__cover_tall_wide">
                                        <img class="show-card__pic" src="<?=CFile::GetPath($arItem['PREVIEW_PICTURE'])?>">
                                    </div>
                                    <div class="show-card__info">
                                        <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?endforeach?>
                </div>
            </div>
        </div>
    <?endif?>
</div>
