<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<div class="terms__sidebar">
    <?foreach ($arResult as $item):?>
        <div class="terms__link">
            <?if ($item['SELECTED']):?>
                <span><?=$item['TEXT']?></span>
            <?else:?>
                <a href="<?=$item['LINK']?>"><?=$item['TEXT']?></a>
            <?endif?>
        </div>
    <?endforeach?>
</div>