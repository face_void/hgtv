<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arTemplateParameters = array(
	"SHOW_INPUT" => array(
		"NAME" => 'Показывать форму ввода поискового запроса',
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
		"REFRESH" => "Y",
	),
	"INPUT_ID" => array(
		"NAME" => 'ID строки ввода поискового запроса',
		"TYPE" => "STRING",
		"DEFAULT" => "title-search-input",
	),
	"CONTAINER_ID" => array(
		"NAME" => 'ID контейнера, по ширине которого будут выводиться результаты',
		"TYPE" => "STRING",
		"DEFAULT" => "title-search",
	),
);
?>
