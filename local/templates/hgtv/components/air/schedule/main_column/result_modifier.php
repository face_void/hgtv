<?
$curTime = date( $arResult['TIME_FORMAT'] );

$arNewResult = [];
$k = 1;
foreach ($arResult['SCHEDULE'][ date($arResult['DAY_FORMAT']) ] as $broadcast) {
    if ($broadcast['TIME_START'] > $curTime || $broadcast['TIME_END'] > $curTime) {
        if ($k > 3) break;

        $arNewResult[] = $broadcast;
        $k++;
    }
}

$arResult['SCHEDULE'] = $arNewResult;