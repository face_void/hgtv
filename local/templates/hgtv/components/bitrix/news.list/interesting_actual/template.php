<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);

$arItems = $arResult['ITEMS'];
?>
<div class="shows__wrapper">
    <?if (count($arItems) > 0):?>
        <div class="shows__column shows__column_1">
            <?
            $arItem = $arItems[0];
            if (!empty($arItem)):?>
                <div id="<?=$arItem['EDIT_ID']?>" class="show-card show-card_1 show-card_bottom-bordered">
                    <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <div class="show-card__cover show-card__cover_tall">
                            <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                        </div>
                        <div class="show-card__info">
                            <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                        </div>
                    </a>
                </div>
            <?
            endif;

            $arItem = $arItems[1];
            if (!empty($arItem)):?>
                <div id="<?=$arItem['EDIT_ID']?>" class="show-card show-card_2">
                    <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <div class="show-card__cover show-card__cover_tall-2">
                            <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                        </div>
                        <div class="show-card__info">
                            <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                        </div>
                    </a>
                </div>
            <?endif?>
        </div>
    <?endif?>
    <?if (count($arItems) > 2):?>
        <div class="shows__column shows__column_2">
            <?
            $arItem = $arItems[2];
            if (!empty($arItem)):?>
                <div id="<?=$arItem['EDIT_ID']?>" class="show-card show-card_3 show-card_bottom-bordered">
                    <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <div class="show-card__cover show-card__cover_tall-2">
                            <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                        </div>
                        <div class="show-card__info">
                            <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                        </div>
                    </a>
                </div>
            <?
            endif;

            $arItem = $arItems[3];
            if (!empty($arItem)):?>
                <div id="<?=$arItem['EDIT_ID']?>" class="show-card show-card_4">
                    <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <div class="show-card__cover show-card__cover_tall">
                            <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                        </div>
                        <div class="show-card__info">
                            <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                        </div>
                    </a>
                </div>
            <?endif?>
        </div>
    <?endif?>
    <?if (count($arItems) > 4):?>
        <div class="shows__column shows__column_3">
            <?
            $arItem = $arItems[4];
            if (!empty($arItem)):?>
                <div id="<?=$arItem['EDIT_ID']?>" class="show-card show-card_5 show-card_bottom-bordered">
                    <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <div class="show-card__cover show-card__cover_wide">
                            <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                        </div>
                        <div class="show-card__info">
                            <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                        </div>
                    </a>
                </div>
            <?endif?>
            <?if (count($arItems) > 5):?>
                <div class="shows__bot-two">
                    <?
                    $arItem = $arItems[5];
                    if (!empty($arItem)):?>
                        <div id="<?=$arItem['EDIT_ID']?>" class="show-card show-card_6">
                            <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                <div class="show-card__cover show-card__cover_last">
                                    <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                                </div>
                                <div class="show-card__info">
                                    <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                                </div>
                            </a>
                        </div>
                    <?
                    endif;

                    $arItem = $arItems[6];
                    if (!empty($arItem)):?>
                        <div id="<?=$arItem['EDIT_ID']?>" class="show-card show-card_7">
                            <a class="show-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                <div class="show-card__cover show-card__cover_last">
                                    <img class="show-card__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                                </div>
                                <div class="show-card__info">
                                    <div class="show-card__name title title_h4"><?=$arItem['NAME']?></div>
                                </div>
                            </a>
                        </div>
                    <?endif?>
                </div>
            <?endif?>
        </div>
    <?endif?>
</div>