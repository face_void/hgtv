<?
// Категории для поиска
$arSearchCat = [
    '2' => ['TITLE' => 'Видео', 'ITEMS' => []],
    '3' => ['TITLE' => 'Шоу', 'ITEMS' => []],
    '8' => ['TITLE' => 'Статьи', 'ITEMS' => []],
];

if (!empty($arResult['SEARCH'])) {
    foreach ($arResult['SEARCH'] as $item) {
        if (array_key_exists($item['PARAM2'], $arSearchCat)) {
            $arSearchCat[$item['PARAM2']]['ITEMS'][] = $item['ITEM_ID'];
        }
    }

    foreach ($arSearchCat as $iBlockId => &$searchCat) {
        if (!empty($searchCat['ITEMS'])) {
            $rsItems = CIBlockElement::GetList(
                ['DATE_ACTIVE' => 'DESC', 'ID' => 'DESC'],
                ['IBLOCK_ID' => $iBlockId, 'ID' => $searchCat['ITEMS']]
            );

            $arItems = [];
            while ($arItem = $rsItems->GetNextElement()) {
                $newItem = $arItem->GetFields();
                $newItem['PROPERTIES'] = $arItem->GetProperties();
                $arItems[] = $newItem;
            }

            $searchCat['ITEMS'] = $arItems;
        }
    }

    $arResult['SEARCH_RESULT'] = $arSearchCat;
}

//\Air\Site\Printer::dbg($arResult);