$(document).ready(function () {
    $(document).on('click', '[data-ajax-popular-video]', function (e) {
        e.preventDefault();
        var $this = $(this),
            container = $('.new-video--popular');

        $this.closest('.ajax-navigation').append('<div class="popular-video__preloader-wrap"><div class="popular-video__preloader-shadow"></div><div class="popular-video__preloader preloader"><div class="lds-dual-ring"></div></div></div>')

        $.ajax({
            url: $this.attr('href'),
            dataType: 'html',
            success: function (result) {
                $this.closest('.video-list__more').remove();
                $('.popular-video__preloader-wrap').remove();
                container.append(result);
            }
        })
    })
});