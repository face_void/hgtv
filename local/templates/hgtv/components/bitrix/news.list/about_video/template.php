<?
$this->setFrameMode(true);
?>
<?if (!empty($arResult['ITEMS'])):?>
    <?
    $arItem = $arResult['ITEMS'][0];

    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK']);
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));
    ?>
    <a id="<?=$this->GetEditAreaID($arItem['ID'])?>" class="about-life__video" href="<?=$arItem['PROPERTIES']['VIDEO_LINK']['VALUE']?>" data-lity>
        <div class="about-life__content">
            <img class="about-life__img" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
            <div class="about-life__play">
                <svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle opacity="0.8" cx="32.5" cy="32.5" r="32.5" fill="white"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M38 32.7083L29.6667 25.4166V40L38 32.7083Z" fill="#3CB9D2"/>
                </svg>
            </div>
            <div class="about-life__bottom">
                <h2 class="about-life__title"><?=$arItem['NAME']?></h2>
                <?if (!empty($arItem['PROPERTIES']['SUBTITLE']['VALUE'])):?>
                    <p class="about-life__note"><?=$arItem['PROPERTIES']['SUBTITLE']['VALUE']?></p>
                <?endif?>
            </div>
        </div>
    </a>
<?endif?>