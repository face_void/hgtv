<?
// Ведущий
if (!empty($arResult['PROPERTIES']['TV_PRESENTER']['VALUE'])) {
    $arPresenter = CIBlockElement::GetByID($arResult['PROPERTIES']['TV_PRESENTER']['VALUE'])->GetNextElement();

    $arResult['PROPERTIES']['TV_PRESENTER']['DETAIL'] = $arPresenter->GetFields();
    $arResult['PROPERTIES']['TV_PRESENTER']['DETAIL']['PROPERTIES'] = $arPresenter->GetProperties();
}

// Слайдер
if (!empty($arResult['PROPERTIES']['SLIDER']['VALUE'])) {
    $rsSlides = CIBlockElement::GetList(
        ['SORT' => 'ASC', 'ID' => 'DESC'],
        [
            'IBLOCK_ID' => $arResult['PROPERTIES']['SLIDER']['LINK_IBLOCK_ID'],
            'SECTION_ID' => $arResult['PROPERTIES']['SLIDER']['VALUE']
        ]
    );

    while ($arSlide = $rsSlides->GetNextElement()) {
        $slide = $arSlide->GetFields();
        $slide['PROPERTIES'] = $arSlide->GetProperties();

        $arResult['PROPERTIES']['SLIDER']['ITEMS'][] = $slide;
    }
}

// Блок "тебе понравится"
if (!empty($arResult['PROPERTIES']['YOU_LIKE']['VALUE'])) {
    $rsRecommend = CIBlockElement::GetList(
        ['SORT' => 'ASC', 'ID' => 'DESC'],
        ['IBLOCK_ID' => $arResult['IBLOCK_ID'], 'ID' => $arResult['PROPERTIES']['YOU_LIKE']['VALUE'], 'ACTIVE' => 'Y'],
        false,
        ['nTopCount' => 2],
        ['ID', 'NAME', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE']
    );

    while ($arRecommend = $rsRecommend->GetNext()) {
        $arResult['PROPERTIES']['YOU_LIKE']['ITEMS'][] = $arRecommend;
    }
}